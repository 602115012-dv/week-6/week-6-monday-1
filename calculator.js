var lastElementIsOperation = false;
function addElement(element) {
  if (!lastElementIsOperation || element != " + " && element != " - " && 
  element != " x " && element != " ÷ ") {
    document.getElementById("result").append(element);
  }
  if (element === " + " || element === " - " || 
  element === " x " || element === " ÷ ") {
    lastElementIsOperation = true;
  } else {
    lastElementIsOperation = false;
  }

}

function calculate() {
  let expression = document.getElementById("result").innerHTML;

  expression = expression.replace("x", "*");
  expression = expression.replace("÷", "/");

  console.log(expression);
  document.getElementById("result").innerHTML = eval(expression);
}

function clearScreen() {
  document.getElementById("result").innerHTML = "&ensp;";
}